package uz.azn.lesson27

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.view.LayoutInflater
import android.widget.Button
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import uz.azn.lesson27.R.id.*

import uz.azn.lesson27.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val adapter  = FragmentAdapter(supportFragmentManager,this)
        adapter.addFragment(FirstFragment(),"Kunlik")
        adapter.addFragment(SecondFragment(),"Haftalik")
        adapter.addFragment(ThirdFragment(),"Oylik")
        adapter.addFragment(FourFragment(),"Haftalik")
        binding.viewPager.adapter = adapter
        val days = findViewById<Button>(R.id.days)
        val week = findViewById<Button>(R.id.week)
        val month = findViewById<Button>(R.id.month)
        val years = findViewById<Button>(R.id.years)

        binding.viewPager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                when(position){
                    0 ->{

                        days.setBackgroundColor(Color.WHITE)
                        days.setTextColor(Color.parseColor("#FF33B5E5"))
                           week.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                week.setTextColor(Color.WHITE)
                                month.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                month.setTextColor(Color.WHITE)
                                years.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                years.setTextColor(Color.WHITE)
                    }
                    1 ->{
                        val b = findViewById<Button>(R.id.week)
                       week.setBackgroundColor(Color.WHITE)
                        week.setTextColor(Color.parseColor("#FF33B5E5"))
                                days.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                days.setTextColor(Color.WHITE)
                                month.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                month.setTextColor(Color.WHITE)
                                years.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                years.setTextColor(Color.WHITE)
                    }
                    2 ->{
                        val b = findViewById<Button>(R.id.month)
                        month.setBackgroundColor(Color.WHITE)
                        month.setTextColor(Color.parseColor("#FF33B5E5"))

                           days.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                days.setTextColor(Color.WHITE)
                                week.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                week.setTextColor(Color.WHITE)
                               years.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                years.setTextColor(Color.WHITE)

                    }

                    3 ->{
                        val b = findViewById<Button>(R.id.years)
                       years.setBackgroundColor(Color.WHITE)
                        years.setTextColor(Color.parseColor("#FF33B5E5"))
                             days.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                days.setTextColor(Color.WHITE)
                                week.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                week.setTextColor(Color.WHITE)
                                month.setBackgroundColor(Color.parseColor("#FF33B5E5"))
                                month.setTextColor(Color.WHITE)
                    }
                }


            }


            override fun onPageSelected(position: Int) {
            }
        })


        days.setOnClickListener {
            binding.viewPager.currentItem = 0
            val fragment = FirstFragment()
            val manager = supportFragmentManager
            manager.beginTransaction().add(R.id.frameLayout,fragment).commit()

        }
        week.setOnClickListener {
            binding.viewPager.currentItem = 1
            val fragment = SecondFragment()
            val manager = supportFragmentManager
            manager.beginTransaction().add(R.id.frameLayout,fragment).commit()

        }
        month.setOnClickListener {
            binding.viewPager.currentItem = 2
            val fragment = ThirdFragment()
            val manager = supportFragmentManager
            manager.beginTransaction().add(R.id.frameLayout,fragment).commit()
        }
        years.setOnClickListener {
            binding.viewPager.currentItem = 3
            val fragment = FourFragment()
            val manager = supportFragmentManager
            manager.beginTransaction().add(R.id.frameLayout,fragment).commit()
        }


    }

// Bu customTablayout yasashni oson usuli
//    fun setMarginOnTabItems(){
//        for(i in 0 until tabLayout.tabCount){
//            val tabItem = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
//            val params = tabItem.layoutParams as ViewGroup.MarginLayoutParams
//            params.setMargins(30, 0, 30, 0)
//        }
    }

