package uz.azn.lesson27

import android.content.Context
import android.view.LayoutInflater
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.lang.StringBuilder

class FragmentAdapter( fragmentManager: FragmentManager, val context: Context):FragmentPagerAdapter(fragmentManager) {

    val fragmentList:MutableList<Fragment> = arrayListOf()
    val titles :MutableList<String> =  arrayListOf()
    override fun getItem(position: Int): Fragment  = fragmentList[position]

    override fun getCount(): Int  = fragmentList.size


    override fun getPageTitle(position: Int): CharSequence?  = titles[position]

    fun addFragment ( fragemts:Fragment, list:String){
        fragmentList.add(fragemts)
        titles.add(list)

    }




}